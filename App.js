import React, { Component } from 'react';
import { StyleSheet, Text, View,StatusBar } from 'react-native';
import  {createAppContainer,createSwitchNavigator, NavigationAction, NavigationProvider} from  "react-navigation"
import Home from './pages/home';
import { Transition } from 'react-native-reanimated';


import Library from './pages/library';
import Header from './pages/Header';
import LibraryMN from './pages/libraryMN';
// import Speak from './pages/speak'
import Bookdetail from './pages/Bookdetail';
import EnBookdetail from './pages/EnBookdetail';
import NavigatorService from './navigator';


const SwitchNav = createSwitchNavigator({
  "Home" : Home,
  "Library": Library,
  "LibraryMN" : LibraryMN,
  "Book" : Bookdetail, 
  "BookGadaad" : EnBookdetail 
},{
  transition: (
    <Transition.Together>
      <Transition.Out
        type="slide-right"
        durationMs={800}
        interpolation="easeIn"
      />
      <Transition.In type="fade" durationMs={800} />
    </Transition.Together>
  ),
  transitionViewStyle: { backgroundColor: 'white' },

})

const Navigator = createAppContainer(SwitchNav);



export default class App extends Component {
state={
  title: "home",
  back: false
} 


render(){  
  const {title, back} = this.state;
  return (
    <View style={{flex:1}}>
      <Header/>
         <StatusBar backgroundColor="blue" barStyle="dark-content" />
         <Navigator ref={navigatorRef => NavigatorService.setContainer(navigatorRef)} />
    </View>
  );
  }
}

