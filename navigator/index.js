import { NavigationActions } from 'react-navigation'; 


let Container;

function setContainer(container) {
  Container = container;
}

 

export function navigate(routeName, params) {
  Container.dispatch(NavigationActions.navigate({
    type: 'Navigation/NAVIGATE',
    routeName,
    params,
  }));
}

export function goBack(key) {
  console.log("going back");
  Container.dispatch(NavigationActions.back({key}));
}

 

export function getCurrentRoute() {
  if (!Container || !Container.state.nav) {
    return null;
  }

  return Container.state.nav.routes[Container.state.nav.index] || null;
}

export const getParams = props => props.navigation.state.params;

export default {
  setContainer, 
  navigate,
  goBack, 
  getCurrentRoute,
};
