import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { Card } from "react-native-paper";
import { Button, List,TouchableRipple } from "react-native-paper";
import Books from "./json/mongolbooks";
import StarRating from './components/star';
// import {navigate} from "../navigator/navigater";


const WIDTH = Dimensions.get("window").width;
const HEIGHT = Dimensions.get("window").height;

export default class library extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Books,
    };

  }


  render() {
    const { Books } = this.state;
    return (
      <ScrollView style={{padding: 10,}}>
        <Text>Монгол номууд</Text> 
        {Books.map((book, i) => (i < 20 ? this.singleBook(book,i) : false))}
      </ScrollView>
    );
  }



  singleBook(book,i) {
    return (
      <View style={styles.book} key={i}>
        <View style={styles.imageStyle}>
          <Image
            source={{ uri: book.src }}
            resizeMode={"contain"}
            style={styles.bookCover}
          />
        </View>
        <View style={{flex:1, marginLeft:50, justifyContent: "center" }}>
          <Text
            adjustsFontSizeToFit={true}
            numberOfLines={2}
            style={{ textAlign: "center", lineHeight:30, fontSize : 16 }}
          >
            {book.title}
          </Text>
          <Text
            adjustsFontSizeToFit={true}
            numberOfLines={2}
            style={{ textAlign: "center", color : "red", lineHeight:30,fontSize : 20,}}
          >
            {book.price}
          </Text>
          <View  style={{alignItems: 'center', paddingVertical: 5,}}>
           <StarRating
         
              rating={book.star}
              size={[20,20]}
            /> 
            <TouchableOpacity style={{marginTop: 10, fontSize: 18}} onPress={()=>this.props.navigation.navigate("Book", {book})}>
      <Text>Номын тухай</Text>
      </TouchableOpacity>

            </View>
        </View>
      </View>
    );
  };

}

const styles = StyleSheet.create({
  textstyle: {
    color: "red",
  },
  imageStyle:{
    alignItems: "flex-start",
      flex:1,
      shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.53,
    shadowRadius: 13.97,
    
    elevation: 21,
  },
  book: {
    flex: 1,
    flexDirection: "row",
    
    
    paddingTop : 30,
    justifyContent: "center",
  },

  bookCover: {
    width: WIDTH / 2,
    height: HEIGHT /3,
    padding: 0,
    margin : 0,
  },
});
