import React, { Component } from 'react';
import { View, Text,StyleSheet } from 'react-native';
import {Button} from "react-native-paper";
import {navigate} from '../navigator';

export default class home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
 

  render() {
    const {navigate} = this.props.navigation; 
    return (
      <View style={styles.container}>
        <Text style={{fontSize : 32}}>welcome to MOODIS</Text>
      <Button mode="outlined" style={{marginTop: 30}} onPress={()=>navigate("Library")}>
        <Text style={styles.textstyle}>Гадаад номууд</Text>
        </Button>
        <Button mode="outlined" style={{marginTop: 30}} onPress={()=>navigate("LibraryMN")}>
        <Text style={styles.textstyle}>Монгол номууд</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
      justifyContent: "center",
      flex:1,
      alignItems: "center"
    },
    textstyle:{
      color : "red",
    }
  });