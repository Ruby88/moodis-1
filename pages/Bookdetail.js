import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Button as Btn,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { Button, List, TouchableRipple, Card } from "react-native-paper";
import StarRating from "./components/star";

import Touchable from 'react-native-platform-touchable';

import * as Speech from 'expo-speech';

import { Audio } from 'expo-av';

// import {navigate} from "../navigator/navigater";

const WIDTH = Dimensions.get("window").width;
const HEIGHT = Dimensions.get("window").height;

export default class Book extends Component {

  soundObject = new Audio.Sound();
  state = {
    selectedExample: "Hello world ",
    inProgress: false,
    pitch: 1.2,
    rate: 0.8,
  };

  _speak = (aboutText) => {
    const start = () => {
      this.setState({ inProgress: true });
    };
    const complete = () => {
      this.state.inProgress && this.setState({ inProgress: false });
    };

    Speech.speak(aboutText, {
      language: "en",
      pitch: this.state.pitch,
      rate: this.state.rate,
      onStart: start,
      onDone: complete,
      onStopped: complete,
      onError: complete,
    });
  };

  _stop = () => {
    Speech.stop();
  };


  async playSound(file){
      try {
        await this.soundObject.loadAsync(file);
        await this.soundObject.playAsync();
        // Your sound is playing!
      } catch (error) {
        // An error occurred!
      }
  }


  async stopSound(){
    try {
      await this.soundObject.pauseAsync()
    } catch (error) {
      // An error occurred!
    }
   
  }

  render() {
    const { book } = this.props.navigation.state.params;
    return (
      <ScrollView style={{ padding: 10 }}> 
        <Text
          style={{
            textAlign: "center",
            marginTop: 10,
            color: "darkslateblue",
            fontSize: 18,
          }}
        >
          Гэр бүл хувь хүний хөгжил
        </Text>
        <View style={styles.book}>
          <View style={styles.imageStyle}>
            <Image
              source={{ uri: book.src }}
              resizeMode={"stretch"}
              style={styles.bookCover}
            />
          </View>
          <View
            style={{ flex: 1, marginLeft: 50, justifyContent: "flex-start" }}
          >
            <View style={styles.suudertei}>
              <Text
                adjustsFontSizeToFit={true}
                numberOfLines={2}
                style={{
                  textAlign: "right",
                  lineHeight: 20,
                  fontWeight: "bold",
                  fontSize: 20,
                }}
              >
                {book.title}
              </Text>
            </View>



            <View style={{ alignItems: "flex-end" }}>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.label}>Хавтас:</Text><Text style={styles.bold}>{book.cover}</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text  style={styles.label}>Хэвлэгдсэн он: </Text>
                <Text style={styles.bold}>2020</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text  style={styles.label}> Хуудас: </Text>
                <Text style={styles.bold}>{book.pages}</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text  style={styles.label}> Хэмжээ:</Text><Text style={styles.bold}>{book.size}</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text  style={styles.label}> Үлдэгдэл: </Text>
                <Text style={styles.bold}>{book.total}</Text>
              </View>
            </View>
            <Text
              adjustsFontSizeToFit={true}
              numberOfLines={2}
              style={{
                textAlign: "right",
                color: "red",
                lineHeight: 30,
                fontSize: 20,
              }}
            >
              {book.price}
            </Text>
            <View style={{ alignItems: "center", paddingTop: 30 }}>
              <StarRating rating={book.star} size={[20, 20]} />
            </View>
          </View>
        </View>
        <View style={styles.controlRow}>
          <Btn
            disabled={this.state.inProgress}
            style={{borderWidth: 1, borderColor: "blue",}}
            // onPress={() => this._speak(book.about)}
            onPress={() => this.playSound(book.audioFile)}
            title="Тухайг унш"
          />

          <Btn
            // disabled={!this.state.inProgress}

            style={{borderWidth: 1, borderColor: "blue",}}
            onPress={async ()=>await this.soundObject.pauseAsync()}
            title="Зогс"
          />
        </View>
        <View>
          <Text
            style={{
              marginTop: 5,
              color: "black",
              lineHeight: 18,
              fontSize: HEIGHT > 800 ? 16 : 12,
            }}
          >
            {book.about}
          </Text>

        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  textstyle: {
    color: "red",
  },
  label: {
    fontSize: 15,
    letterSpacing: 1,
    lineHeight : 26,
  },
  bold: {
    fontSize: 15,

    lineHeight : 26,
    fontWeight: "bold",
  },
  imageStyle: {
    alignItems: "flex-start",
    flex: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.53,
    shadowRadius: 13.97,

    elevation: 21,
  },
  suudertei: {
    shadowColor: "#000",
    shadowOffset: {
      width: 30,
      height: 10,
    },
    shadowOpacity: 0.53,
    shadowRadius: 13.97,
  },
  book: {
    flex: 1,
    flexDirection: "row",

    paddingTop: 30,
    justifyContent: "center",
  },

  bookCover: {
    width: WIDTH / 2,
    height: HEIGHT / 3,
    padding: 0,
    margin: 0,
  },

  controlRow: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
});
