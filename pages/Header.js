import * as React from 'react';
import { Appbar } from 'react-native-paper';
import NavigatorService from '../navigator'; 

export default class MyComponent extends React.Component {

   state = {
     back: true
   }
 

  _goBack = () => {
    
    let route = NavigatorService.getCurrentRoute().key;

    switch(route) {
      case "Home": 
        NavigatorService.navigate("Home");
        break;
      case "Library": 
        NavigatorService.navigate("Home");
        break; 
      case "LibraryMN": 
        NavigatorService.navigate("Home");
        break; 
      case "Book": 
        NavigatorService.navigate("LibraryMN");
        break;
      case "BookGadaad": 
        NavigatorService.navigate("Library");
        break; 

    }
        
  }

  _handleSearch = () => console.log('Searching');

  _handleMore = () => console.log('Shown more');

  render() {
      const {title} = this.props;
    return (
      <Appbar.Header>
          {this.state.back &&   <Appbar.BackAction
          onPress={this._goBack}
        />
          }
      
        <Appbar.Content style={{alignItems: "center"}}
          title={title}
        />
        <Appbar.Action icon="magnify" onPress={this._handleSearch} />
        <Appbar.Action icon="dots-vertical" onPress={this._handleMore} />
      </Appbar.Header>
    );
  }
}