export default [
    { "src": "https://book.mn/uploads/products/1526639120-11136562.jpg",
    "author": "Maggie stiefvater",
    "title": "The scorpio races ",
    "price": "30,000 ₮",
    "size" : "205*145мм",
    "star" : 4,
    "cover" : "Зөөлөн",
    "total" : 100,
    "pages" : "484",

        "about" : `A spellbinding novel from #1 New York Times bestselling author Maggie Stiefvater.

        Some race to win. Others race to survive.
        
        It happens at the start of every November: the Scorpio Races. Riders attempt to keep hold of their water horses long enough to make it to the finish line.
        Some riders live.
        Others die.
        At age nineteen, Sean Kendrick is the returning champion. He is a young man of few words, and if he has any fears, he keeps them buried deep, where no one else can see them.`
    },
]