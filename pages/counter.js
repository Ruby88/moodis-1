import React, { Component } from 'react';
import { View, Text,StyleSheet } from 'react-native';
import {Button} from "react-native-paper";

export default class home extends Component {
  state = {
      counter: 1,
      totalVote: 231,
  }


  render() {
    const {buttonText, color} = this.props;
    const {counter,totalVote} = this.state;

    return (
      <View>
        <Text style={{fontSize : 32}}> welcome to my  app!  </Text>
        <Text style={{fontSize : 32}}> Total vote: {totalVote}  </Text>
      

        <Text style={{fontSize : 32, textAlign: "center"}}> {counter}  </Text>
      <Button mode="outlined" style={{marginTop: 30}} onPress={()=>this.setState({counter: counter -1})}>
               <Text style={[styles.textstyle, {color: color}]}>{buttonText}</Text>
      </Button>
      <Button mode="outlined" style={{marginTop: 30}} onPress={()=>this.setState({totalVote:0})}>
               <Text style={[styles.textstyle, {color: "red"}]}>Reset vote</Text>
      </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    textstyle:{
      color : "red",
    }
  });